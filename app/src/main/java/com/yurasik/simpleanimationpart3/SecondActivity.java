package com.yurasik.simpleanimationpart3;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.yurasik.simpleanimationpart3.utils.Constants;
import com.yurasik.simpleanimationpart3.utils.ThemesChange;

import butterknife.ButterKnife;

public class SecondActivity extends Activity {

    public static void launch(Context context, boolean day){
        Intent intent = new Intent(context, SecondActivity.class);
        intent.putExtra(Constants.THEME, day);
        intent.setAction(Constants.THEME);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ThemesChange.setTheme(SecondActivity.this, getIntent());
        setContentView(R.layout.activity_second);
        ButterKnife.bind(this);
        final Button button = (Button) findViewById(R.id.BackButton);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = AnimationUtils.loadAnimation(SecondActivity.this, R.anim.grow_spin);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        button.setVisibility(View.INVISIBLE);
                        goBack();
                    }
                });
                button.startAnimation(animation);
            }
        });
    }

    @Override
    protected void onResume() {
        findViewById(R.id.BackButton).setVisibility(View.VISIBLE);
        super.onResume();
    }

    private void goBack() {
        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            goBack();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }
}
