package com.yurasik.simpleanimationpart3.utils;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;

import com.yurasik.simpleanimationpart3.R;

/**
 * Created by Юрий on 21.09.2015.
 */
public class ThemesChange {

    public static void setTheme(Activity activity, Intent args){
        boolean day = true;
        if (Constants.THEME.equals(args.getAction())) {
            day = args.getBooleanExtra(Constants.THEME, true);
            Log.d("Input day", "day = " + day);
        } else {
            day = SettingsProvider.getInstance(activity).getTheme();
        }
        if (day) activity.setTheme(R.style.MyThemeDay);
        else activity.setTheme(R.style.MyThemeNight);
    }
}
