package com.yurasik.simpleanimationpart3.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.appcompat.BuildConfig;

/**
 * Created by Юрий on 21.09.2015.
 */
public class SettingsProvider {

    private static SettingsProvider settingsProvider;
    private static SharedPreferences preferences;

    public static SettingsProvider getInstance(Context context){
        if (settingsProvider==null) {
            settingsProvider = new SettingsProvider();
            preferences = context
                    .getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
        }
        return settingsProvider;
    }

    public boolean getTheme(){
        return preferences.getBoolean(Constants.THEME, true);
    }

    public void setTheme(boolean day){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Constants.THEME, day);
        editor.commit();
    }
}
