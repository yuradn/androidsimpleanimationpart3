package com.yurasik.simpleanimationpart3;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.text.style.StyleSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.TextView;

import com.yurasik.simpleanimationpart3.utils.Constants;
import com.yurasik.simpleanimationpart3.utils.SettingsProvider;
import com.yurasik.simpleanimationpart3.utils.ThemesChange;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FirstActivity extends Activity {
    @Bind(R.id.tvFirst)
    protected TextView tvFirst;
    @Bind(R.id.tvSecond)
    protected TextView tvSecond;

    public static void launch(Context context, boolean day) {
        Intent intent = new Intent(context, FirstActivity.class);
        intent.putExtra(Constants.THEME, day);
        intent.setAction(Constants.THEME);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ThemesChange.setTheme(FirstActivity.this, getIntent());
        setContentView(R.layout.activity_first);
        ButterKnife.bind(this);
        setSpanToTvFirst();
        setSpanToTvSecond();
    }

    private void setSpanToTvSecond() {
        SpannableStringBuilder ssb = new SpannableStringBuilder("Here's a smiley  , and here's a link http://blog.stylingandroid.com");
        Bitmap smiley = BitmapFactory.decodeResource(getResources(), android.R.drawable.ic_media_play);
        ssb.setSpan(new ImageSpan(smiley), 16, 17, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        tvSecond.setText(ssb, TextView.BufferType.SPANNABLE);
        Linkify.addLinks(tvSecond, Linkify.WEB_URLS);
    }

    private void setSpanToTvFirst() {
        Spannable spannable = (Spannable) tvFirst.getText();
        StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
        spannable.setSpan(boldSpan, 41, 52, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
    }

    @OnClick(R.id.btnNextButton)
    protected void toSecondActivity(final View button) {
        Animation animation = AnimationUtils.loadAnimation(FirstActivity.this, R.anim.fade_out);
        animation.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                button.setVisibility(View.INVISIBLE);
                SecondActivity.launch(FirstActivity.this,
                        SettingsProvider.getInstance(FirstActivity.this).getTheme());
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });
        button.startAnimation(animation);
    }

    @OnClick(R.id.btnChangeThemeDay)
    protected void changeThemeToDay() {
        if (!SettingsProvider.getInstance(FirstActivity.this).getTheme()) {
            Log.d("Change theme", "Change theme to day");
            SettingsProvider.getInstance(FirstActivity.this).setTheme(true);
            FirstActivity.launch(FirstActivity.this, true);
            finish();
        }
    }

    @OnClick(R.id.btnChangeThemeNight)
    protected void changeThemeToNight() {
        if (SettingsProvider.getInstance(FirstActivity.this).getTheme()) {
            Log.d("Change theme", "Change theme to night");
            SettingsProvider.getInstance(FirstActivity.this).setTheme(false);
            FirstActivity.launch(FirstActivity.this, false);
            finish();
        }
    }

    @OnClick(R.id.btnTest)
    protected void testButton() {
        Log.d("Test", "OnClick!");
    }

    @Override
    protected void onResume() {
        findViewById(R.id.btnNextButton).setVisibility(View.VISIBLE);
        super.onResume();
    }
}
